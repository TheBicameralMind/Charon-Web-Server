# Charon-Web-Server
Welcome to the sparse configuration of my amateur web server, which has since
been lost to an unfortunate hard drive failure. It ran Ubuntu 16.04 LTS, and 
served [Charon Web](https://github.com/YnganuAst/Charon-Web-Frontend) using 
Express.js. In addition to the web server, I configured dynamic DNS, port 
forwarding, and SSH using key-based authentication for this server, as well as
building several 
[Python scripts](https://gitlab.com/TheBicameralMind/Music-Library-Utilities) to
 manage the media files being served. 