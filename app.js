var express = require('express');
var fs = require('fs');
var path = require('path');
var helmet = require('helmet');
var morgan = require('morgan');
var rfs = require('rotating-file-stream');

var app = express();

// security(?)
app.use(helmet());

// gimme that hot compressed rotating file stream i guess
var fileStream = rfs('webserver.log', {
    interval: '7d',
    path: path.join(__dirname, 'logs'),
    compress: 'gzip'
});

// apache common logs for convenience
app.use(morgan('common', {
    stream: fileStream,
    skip: function(req, res) {
	return res.statusCode > 400;
    }
}));

// CHARON WEB
// serve index page of charon web
app.get('/charon-web', function(req, res) {
    res.sendFile(path.join(__dirname, 'Charon-Web-Frontend/index.html'));
});

// statically serve scripts and styles
app.use('/charon-web/scripts', express.static(path.join(__dirname, 'Charon-Web-Frontend/scripts')));
app.use('/charon-web/styles', express.static(path.join(__dirname, 'Charon-Web-Frontend/styles')));

// statically serve json song database
// app.use('/charon-web/song-json', express.static('/home/ftpclient/ftp/musicDatabase.json'));

// serve music directory statically
// app.use('/charon-web/song/', express.static('/home/ftpclient/ftp/files'));

// statically serve metadata
app.use('/charon-web/metadata', express.static('/home/ftpclient/ftp_cp/metadata'));

// dynamically serve collection songs
app.get('/charon-web/collections/:col_enc/songs/:song_enc', function(req, res) {
    var path = '/home/ftpclient/ftp_cp/collections/' + req.params.col_enc + '/' + req.params.song_enc + '.mp3';
    if (!fs.existsSync(path)) {
	res.status(404).send('404: File not found');
    } else {
	res.sendFile(path);
    }
});

// MINECRAFT MAP
// serve index page of minecraft map
app.get('/minecraft/world-map', function(req, res) {
    var ip = (req.headers['x-forwarded-for'] ||
	      req.connection.remoteAddress ||
	      req.socket.remoteAddress ||
	      req.connection.socket.remoteAddress).split(",")[0];
    var date = new Date().toLocaleString();
//    console.log('[REQ] on ' + date + ' FOR /minecraft/world_map FROM ' + ip;)
    res.sendFile(path.join(__dirname, 'map/index.html'));
});

// statically serve map directory for map index.html to pull other files from
app.use('/minecraft', express.static(path.join(__dirname, 'map')));


// start server
app.listen(8080, function() {
    console.log("App listening on port 8080");
});
